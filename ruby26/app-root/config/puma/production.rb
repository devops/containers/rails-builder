#
# Production puma config for Docker
#
max_t = ENV.fetch('RAILS_MAX_THREADS', '1').to_i
min_t = ( max_t == 1 ? 1 : 0 ) # If max is 1, set min to 1 to disable multi-threading
threads min_t, max_t

# https://github.com/puma/puma/blob/master/docs/deployment.md#mri
n_workers = ENV.fetch('WEB_CONCURRENCY') { (`nproc`.chomp.to_i * 3) / 2 }
workers n_workers.to_i

port ENV.fetch('RAILS_PORT')

preload_app!

# We expect SIGTERM
raise_exception_on_sigterm false
